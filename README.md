# peka-timetables
View bus and trams live timetables in your browser.
Contains the same data, as LCD displays on the stops, with live schedules.

# Example view
<img src="./docs/screen.png"  height="500" >

# Development
As any Python app.
But the code is ugly, because it is old, from Python learning times.

# Deployment with docker
```
cd $PROJECT_DIR
docker build -t peka-timetables-img .
# optionally deploy to your local machine (remember to stop the previous version)
# docker save peka-timetables-img | bzip2 | pv | ssh your-machine@10.11.12.35 docker load
docker run -d --restart unless-stopped --name peka-timetables -p 28368:80 peka-timetables-img
```

Stopping previous container:
```
docker container ls  # to get the CONTAINER_ID
docker stop CONTAINER_ID
docker rm CONTAINER_ID
docker container ls -a  # to get image_ID
docker rmi IMAGE_ID
```
