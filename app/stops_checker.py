import requests
import json
import time
import enum
from dateutil import parser


class PekaServiceError(Exception):
    pass


class StopsSymbol(enum.Enum):
    RONDO_SKUBISZEWSKIEGO = 'ROSK42'
    RONDO_JEZIORANSKIEGO_1 = 'RONJ41'
    RONDO_JEZIORANSKIEGO_2 = 'RONJ42'
    RONDO_JEZIORANSKIEGO_3 = 'RONJ74'
    KORNICKA = 'KORN41'
    BARANIAKA = 'BAKA42'

    @classmethod
    def get_all_symbols(cls):
        return [e.value for e in cls]


class ObservedStop:
    def __init__(self, stop_symbol: StopsSymbol, custom_name: str, only_lines: list = None, excluded_directions: list = None):
        self.stop_symbol = stop_symbol
        self.only_lines = only_lines
        self.custom_name = custom_name
        self.excluded_directions = excluded_directions


arrow_symbol = u'\u2192'
observed_stops_for_symbols = {
    StopsSymbol.RONDO_SKUBISZEWSKIEGO.value: ObservedStop(
        stop_symbol=StopsSymbol.RONDO_SKUBISZEWSKIEGO,
        custom_name='Home' + arrow_symbol + 'City'),
    StopsSymbol.RONDO_JEZIORANSKIEGO_1.value: ObservedStop(
        stop_symbol=StopsSymbol.RONDO_JEZIORANSKIEGO_1,
        custom_name='Work' + arrow_symbol + 'City'),
    StopsSymbol.RONDO_JEZIORANSKIEGO_2.value: ObservedStop(
        stop_symbol=StopsSymbol.RONDO_JEZIORANSKIEGO_2,
        custom_name='Work' + arrow_symbol + 'Home'),
    StopsSymbol.RONDO_JEZIORANSKIEGO_3.value: ObservedStop(
        stop_symbol=StopsSymbol.RONDO_JEZIORANSKIEGO_3,
        custom_name='Work' + arrow_symbol + 'Arena'),
    StopsSymbol.KORNICKA.value: ObservedStop(
        stop_symbol=StopsSymbol.KORNICKA,
        custom_name='PP' + arrow_symbol + 'Home [6]'),
    StopsSymbol.BARANIAKA.value: ObservedStop(
        stop_symbol=StopsSymbol.BARANIAKA,
        custom_name='PP' + arrow_symbol + 'Home [13]'),
}


def request_information_for_stop(stop_symbol: StopsSymbol):
    current_timestamp = int(round(time.time() * 1000))
    address = "https://www.peka.poznan.pl/vm/method.vm?ts=" + str(current_timestamp)
    payload = "method=getTimes&p0=%7B%22symbol%22%3A%22" + stop_symbol.value + "%22%7D"
    raw_response = requests.post(
        address,
        data=payload,
        headers={'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'})

    # print(raw_response.status_code, raw_response.reason)
    if raw_response.status_code != 200:
        exception_message = 'Error during requesting data from peka. Error Code: {err_code}. Reason: {reason}.'.format(
            err_code=str(raw_response.status_code), reason=raw_response.reason)
        raise PekaServiceError(exception_message)
    json_dict = json.loads(raw_response.text)

    if 'success' in json_dict:
        response_dict = json_dict['success']
        return response_dict
    else:
        raise PekaServiceError('Something wrong with response structure!')


def get_formatted_departure(raw_departure):
    date_time = parser.parse(raw_departure)
    output = str(date_time.hour).zfill(2) + ':' + str(date_time.minute).zfill(2)
    return output


def parse_response_dict_for_observed_stop(raw_peka_dict: dict, observed_stop: ObservedStop):
    times_list = raw_peka_dict['times']
    bollard = raw_peka_dict['bollard']

    bollard_short = {'name': bollard['name']}
    times_list_short = []

    for arrival in times_list:
        line_number = int(arrival['line'])
        direction = arrival['direction']

        if observed_stop.excluded_directions and direction in observed_stop.excluded_directions:
            continue  # skip this arrival, because excluded
        if observed_stop.only_lines and line_number not in observed_stop.only_lines:
            continue  # line not observed

        arrival_dict = {
            'departure': get_formatted_departure(arrival['departure']),
            'realTime': arrival['realTime'],
            'minutes': arrival['minutes'],
            'line': arrival['line'],
            'direction': arrival['direction'],
            'onStopPoint': arrival['onStopPoint']
        }
        times_list_short.append(arrival_dict)

    response_dict = {'bollard': bollard_short, 'times': times_list_short, 'custom_name': observed_stop.custom_name}
    return response_dict


def get_data_for_observed_stop(observed_stop: ObservedStop):
    raw_peka_dict = request_information_for_stop(observed_stop.stop_symbol)
    response_dict = parse_response_dict_for_observed_stop(raw_peka_dict=raw_peka_dict, observed_stop=observed_stop)
    response_json = json.dumps(response_dict)
    return response_json


def main():
    observed_stop = observed_stops_for_symbols[StopsSymbol.RONDO_SKUBISZEWSKIEGO.value]
    response_dict = get_data_for_observed_stop(observed_stop=observed_stop)
    print('success!')


if __name__ == '__main__':
    main()
