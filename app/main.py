from stops_checker import get_data_for_observed_stop, StopsSymbol, observed_stops_for_symbols, PekaServiceError

from flask import Flask, redirect, request


app = Flask(__name__)


@app.route('/')
def redirect_1():
    return redirect("/static/index.html", code=302)


@app.route('/index.html')
def redirect_2():
    return redirect("/static/index.html", code=302)


@app.route("/hello")
def hello():
    return "Hello World from Flask"


@app.route("/get-stop-data")
def get_stop_data():
    query_dict = request.args.to_dict()
    query_keys_set = set(query_dict.keys())

    if 'stop' not in query_keys_set:
        return get_bad_request_response('Error! Missing "stop" parameter for "get-stop-data"')
    print('###')
    stop_symbol = query_dict['stop']
    if stop_symbol not in StopsSymbol.get_all_symbols():
        return get_bad_request_response('Unknown stop id for "get-stop-data: "' + str(stop_symbol) + '"')

    try:
        observed_stop = observed_stops_for_symbols[stop_symbol]
        json = get_data_for_observed_stop(observed_stop=observed_stop)
        return json
    except PekaServiceError as e:
        return get_problem_with_peka_response(additional_info=str(e))


def get_bad_request_response(additional_info=""):
    response = 'ERROR! ' + additional_info
    return response, 400


def get_problem_with_peka_response(additional_info=""):
    response = 'ERROR! ' + additional_info
    return response, 503


if __name__ == "__main__":
    # Only for debugging while developing
    app.run(host='0.0.0.0', debug=True, port=8000)
